using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(MouseLook))]
public class PlayerFPSController : MonoBehaviour
{
    //public float walkSpeed = 5f;

    public CharacterMovement characterMovement;
    public MouseLook mouseLook;

    private void start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();

    }

    private void Update()
    {
        movement();
        rotation();
    }

    private void movement()
    {
        //Movement
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        //Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        //transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        //Debug.Log(hMovementInput + " " + vMovementInput + " " + jumpInput + " " + dashInput);

        characterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }

    private void rotation()
    {
        //Rotation
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        mouseLook.handleRotation(hRotationInput, vRotationInput);
    }
}











    /*
    public float walkSpeed = 5f;       // WalkSpeed
    
    void Start () 
    {
     // Find the object with name capsule and disable its meshrenderer

        GameObject.Find("Capsule").gameObject.SetActive(false);  
    }

    void update () 
    {
        movement();
    }

    private void movement () 
    {
        //Movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));

    }


    */






    /*
    public GameObject cam;
    public float walkSpeed = 5f;          // WalkSpeed
    public float hRotationSpeed = 100f;   // Player rotates along y axis
    public float vRotationSpeed = 80f;    // Cam rotates along x axis

    //Use this for initialization
    void Start()
    {
        // Hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);
    }
    */

    /*
    // Update is called once per frame
    void Update()
    {
        // Movement
        /*
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime)); */

        //movement();

        // Rotation
        //float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        //float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        //transform.Rotate(0f, hPlayerRotation, 0f);
       // cam.transform.Rotate(-vCamRotation, 0f, 0f);
    //} 

    //private void movement() 
    //{
        //float hMovement = Input.GetAxisRaw("Horizontal");
        //float vMovement = Input.GetAxisRaw("Vertical");

       // Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        //transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));
    //}

